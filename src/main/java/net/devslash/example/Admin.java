package net.devslash.example;

import com.opensymphony.xwork2.ActionSupport;
import net.devslash.example.Security.SecRun;
import net.devslash.example.Security.Security;

/**
 * Author: PT
 * Date: 30/3/2014
 * Time: 6:38 PM
 */

public class Admin extends ActionSupport {

    @Security(secLevel = Security.LEVEL.ADMIN)
    public String execute(){
        return SUCCESS;
    }

    @Security(secLevel = Security.LEVEL.USER)
    public String something(){return "";}

}
