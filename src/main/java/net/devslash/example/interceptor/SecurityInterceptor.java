package net.devslash.example.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import net.devslash.example.Security.Security;
import net.devslash.example.Security.SecurityRunner;
import org.apache.struts2.ServletActionContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Author: PT
 * Date: 30/3/2014
 * Time: 5:53 PM
 */
public class SecurityInterceptor implements Interceptor {


    @Override
    public void destroy() {

    }

    @Override
    public void init() {

    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {

        Class<?> usedClass = actionInvocation.getAction().getClass();
        String methodName = actionInvocation.getProxy().getMethod();
        Method usedClassMethod = usedClass.getMethod(methodName);

        Annotation[] classAnnotations = usedClass.getDeclaredAnnotations();
        Annotation[] methodAnnotations = usedClassMethod.getDeclaredAnnotations();

        Map<String, Object> session = actionInvocation.getInvocationContext().getSession();

        if (!performSecurity(classAnnotations, session) ||
                !performSecurity(methodAnnotations, session)) {

            if(session.containsKey("username")){
                ServletActionContext.getResponse().sendError(501);
                return "success";
            }

            return "login";
        }
        String invoke = actionInvocation.invoke();
        return invoke;
    }

    private boolean performSecurity(Annotation[] methodAnnotations, Map<String, Object> session) throws IllegalAccessException, InstantiationException {
        Security thisAnno;
        if ((thisAnno = getSecurityAnnotation(methodAnnotations)) != null) {
            Security.LEVEL level = thisAnno.secLevel();
            SecurityRunner fs = thisAnno.secRunner().newInstance();

            if (!fs.isAllowed(level, session)) {
                return false;
            }
        }

        return true;

    }

    private Security getSecurityAnnotation(Annotation[] methodAnnotations) {

        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof Security) {
                return (Security) annotation;
            }
        }
        return null;

    }


}
