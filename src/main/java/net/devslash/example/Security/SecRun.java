package net.devslash.example.Security;

import java.util.Map;

/**
 * Author: PT
 * Date: 30/3/2014
 * Time: 6:24 PM
 */
public class SecRun implements SecurityRunner {

    @Override
    public boolean isAllowed(Security.LEVEL level, Map<String, Object> sessionMap) {

        if(sessionMap.containsKey("username") == false){
            if(level == Security.LEVEL.PUBLIC){
                return true;
            }
            return false;
        }


        if(sessionMap.get("username").equals("paul")){
            return false;
        }

        return true;
    }

}
