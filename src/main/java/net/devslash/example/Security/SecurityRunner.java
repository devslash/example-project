package net.devslash.example.Security;

import java.util.Map;

/**
 * Author: PT
 * Date: 30/3/2014
 * Time: 6:23 PM
 */
public interface SecurityRunner {

    public boolean isAllowed(Security.LEVEL level, Map<String, Object> session);

}
