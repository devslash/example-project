package net.devslash.example.Security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Author: PT
 * Date: 30/3/2014
 * Time: 5:52 PM
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    enum LEVEL {
        USER, OWNER, ADMIN, PUBLIC
    }

    LEVEL secLevel();
    Class<? extends SecurityRunner> secRunner() default SecRun.class;
}
