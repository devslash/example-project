<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Sign On</title>
</head>

<body>

<s:a action="HelloWorld">Back</s:a>

<s:form action="Login">
    <s:textfield key="username"/>
    <s:password  key="password" />
    <s:submit/>
</s:form>

</body>
</html>
