<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Welcome</title>
</head>

<body>
<h2><s:property value="message"/></h2>

<s:a action="Admin">Admin</s:a><br/>

<s:if test="%{#session.username != null}">
You are logged in as <s:property value="%{#session.username}"/>
    <s:a action="Logout">Logout</s:a>
</s:if>
<s:else>
    You are not logged in
    F<s:property value="%{#session.username}"/>F
</s:else>

<h4><s:a action="Login">Login</s:a> </h4>

</body>
</html>
